package com.sunil.repo;

import org.springframework.data.repository.CrudRepository;

import com.sunil.entity.Student;

public interface StudentRepository extends CrudRepository<Student, Integer> {

}
