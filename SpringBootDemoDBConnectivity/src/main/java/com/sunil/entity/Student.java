package com.sunil.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="studtab")
public class Student {
	
	@Id
	@Column(name="sid")
	private Integer studentId;
	@Column(name="stname")
	private String studentName;
	@Column(name="fees")
	private Double fees; 

}
