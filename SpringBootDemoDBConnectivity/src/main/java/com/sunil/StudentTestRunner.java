package com.sunil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.sunil.entity.Student;
import com.sunil.repo.StudentRepository;

@Component
public class StudentTestRunner implements CommandLineRunner {

	@Autowired
	private StudentRepository studentRepository;
	@Override
	public void run(String... args) throws Exception {


		Student s= new Student(101,"Sunil",2500.0);
		studentRepository.save(s);
		System.out.println("Data has been saved successfully !!");

	}

}
